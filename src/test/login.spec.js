/**
 * Created by Jean Carlo Rodriguez on 1/7/2016.
 */
(function () {

    var config = require("../../config.json");

    var Home = require("../pages/home.page.js");
    var LoginForm = require("../pages/loginForm.page.js");
    var Main = require("../pages/main.page.js");
    var PageTransporter = require("../pages/pageTransporter.js")

    describe("Suite Login", function () {
        var home = new Home();
        var loginForm = new LoginForm();
        var main = new Main();
        var pageTransporter = new PageTransporter();

        beforeEach(function () {
            pageTransporter.goToHome();
        });

        it("login in to the page with a valid user", function () {
            home.clickOnLogin();
            loginForm.loginWithValidAccount(config.account.user, config.account.password);
            expect(main.isLeftMenuDisplayed()).toBe(true);
        });

        afterEach(function () {
            main.logout();
        });
    });
})();