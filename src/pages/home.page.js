/**
 * Created by Jean Carlo Rodriguez on 1/7/2016.
 */
(function () {
    var config = require("../../config.json")
    var Home = function () {
        this.loginLink = element(By.linkText("Log in"));
    };

    Home.prototype.clickOnLogin = function () {
        this.loginLink.click();
        browser.sleep(config.SLEEPTIME);
    };
    module.exports = Home;
})();