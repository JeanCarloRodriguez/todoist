/**
 * Created by Jean Carlo Rodriguez on 1/7/2016.
 */
(function () {
    var Main = function () {
        this.leftMenu = element(by.id("left_menu"));
        this.userMenuButton = element(by.css("#gear_holder"));
        this.logoutButton = element(by.cssContainingText("span","Log out"));

        this.EC = protractor.ExpectedConditions;
    };

    Main.prototype.isLeftMenuDisplayed = function () {
        browser.wait(this.EC.visibilityOf(this.leftMenu));
        return this.EC.visibilityOf(this.leftMenu).call();
    };

    Main.prototype.logout = function () {
        this.userMenuButton.click();
        browser.wait(this.EC.elementToBeClickable(this.logoutButton));
        this.logoutButton.click();
    };
    module.exports = Main;
})();