/**
 * Created by Jean Carlo Rodriguez on 1/11/2016.
 */
(function () {
    var config = require("../../config.json")
    var PageTransporter = function () {

    };

    PageTransporter.prototype.goToHome = function () {
        browser.get(config.homeUrl);
    };

    module.exports = PageTransporter;
})();