/**
 * Created by Jean Carlo Rodriguez on 1/7/2016.
 */
(function () {
    var LoginForm = function () {
        this.emailInput = element(by.id("email"));
        this.passwordInput = element(by.id("password"));
        this.loginButton = element(by.linkText("Log in"));
        this.EC = protractor.ExpectedConditions;
    };

    LoginForm.prototype.setEmail = function (email) {
        browser.wait(this.EC.visibilityOf(this.emailInput));
        this.emailInput.clear();
        this.emailInput.sendKeys(email);
    };

    LoginForm.prototype.setPassword = function (password) {
        this.EC.visibilityOf(this.passwordInput).call();
        this.passwordInput.clear();
        this.passwordInput.sendKeys(password);
    };

    LoginForm.prototype.clickOnLoginButton = function () {
        this.loginButton.click();
    };

    LoginForm.prototype.loginWithValidAccount = function (email, password) {
        // switch to the first frame
        browser.driver.switchTo().frame("GB_frame");
        // switch to the second frame
        browser.driver.switchTo().frame("GB_frame");
        this.setEmail(email);
        this.setPassword(password);
        this.clickOnLoginButton();
        browser.driver.switchTo().defaultContent();
    };

    module.exports = LoginForm;
})();